package org.tw.battle.infrastructure;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @author Liu Xia
 */
public class ArgumentParser {
    public static String[] parse(String command) {
        List<String> tokens = new ArrayList<>();
        final StringTokenizer tokenizer = new StringTokenizer(command);
        while (tokenizer.hasMoreTokens()) {
            tokens.add(tokenizer.nextToken());
        }

        return tokens.toArray(new String[0]);
    }
}
