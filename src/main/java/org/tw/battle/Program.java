package org.tw.battle;

import org.tw.battle.domain.ServiceConfiguration;
import org.tw.battle.infrastructure.DatabaseConfiguration;
import org.tw.battle.infrastructure.DatabaseConnectionProvider;

import java.sql.Connection;

/**
 * @author Liu Xia
 *
 * IMPORTANT: You cannot modify this file.
 */
public class Program {
    @SuppressWarnings("FieldCanBeLocal")
    private static Connection keepInMemoryConnection;

    public static void main(String[] args) throws Exception {
        final ServiceConfiguration configuration = new DatabaseConfiguration(
            "jdbc:h2:mem:prodDB;MODE=MYSQL;", "sa", "p@ssword", "org.h2.Driver"
        );

        keepInMemoryConnection = DatabaseConnectionProvider.createConnection(configuration);
        try {
            new GameFacade().run(configuration, System.in, System.out);
        } finally {
            keepInMemoryConnection.close();
        }
    }
}
