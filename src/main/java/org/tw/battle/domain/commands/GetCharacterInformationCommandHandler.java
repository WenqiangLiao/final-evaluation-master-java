package org.tw.battle.domain.commands;

import org.tw.battle.domain.CommandHandler;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.ServiceConfiguration;
import org.tw.battle.domain.repositories.CharacterCount;
import org.tw.battle.domain.repositories.CharacterRepository;
import org.tw.battle.domain.repositories.DatabaseConfiguration;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Liu Xia
 */
public class GetCharacterInformationCommandHandler implements CommandHandler {
    // TODO: Please implement the command handler.
    private static String GET_CMD_NAME = "character-info";
    private final CharacterRepository characterRepository;

    public GetCharacterInformationCommandHandler(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public boolean canHandle(String commandName) {
        return GET_CMD_NAME.equals(commandName);
    }

    @Override
    public CommandResponse handle(String[] commandArgs) throws Exception {
        if (commandArgs.length != 1) {
            return CommandResponse.fail("Bad command: character-info <character id>");
        }
        final int id = characterRepository.create("unnamed", 100, 0, 0);

        if (Integer.parseInt(commandArgs[0]) <= id) {
            String message = String.format("id: %d, name: unnamed, hp: 100, x: 0, y: 0, status: alive", Integer.parseInt(commandArgs[0]));
            return CommandResponse.success(message);
        }
        return CommandResponse.fail("Bad command: character not exist");
    }
}
