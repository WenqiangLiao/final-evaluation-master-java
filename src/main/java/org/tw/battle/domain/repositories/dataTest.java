package org.tw.battle.domain.repositories;

import org.tw.battle.domain.ServiceConfiguration;
import org.tw.battle.domain.repositories.DatabaseConfiguration;
import org.tw.battle.infrastructure.DatabaseConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class dataTest {
    private final ServiceConfiguration configuration;

    public dataTest(ServiceConfiguration configuration) {
        this.configuration = configuration;
    }
    public static void main(String[] args) throws Exception {

        Pattern pattern = Pattern.compile("^character-info [0-9]+");
        Matcher matcher = pattern.matcher("character-info 1");
        System.out.println(matcher.matches());

        ServiceConfiguration configuration = new DatabaseConfiguration(
                "jdbc:h2:mem:prodDB;MODE=MYSQL;", "sa", "p@ssword", "org.h2.Driver"
        );
        dataTest dt = new dataTest(configuration);
        dt.selectData();
    }

    public int selectData() throws Exception {
        Connection connection = null;
        PreparedStatement selectPreparedStatement = null;
        String selectSql = "select * from character";

        try {
            connection = DatabaseConnectionProvider.createConnection(configuration);
//            connection.setAutoCommit(false);

//            insertPreparedStatement = connection.prepareStatement(insertSql);
//            insertPreparedStatement.setString(1, name);
//            insertPreparedStatement.setInt(2, hp);
//            insertPreparedStatement.setInt(3, x);
//            insertPreparedStatement.setInt(4, y);
//
//            insertPreparedStatement.executeUpdate();
//            insertPreparedStatement.close();
//            connection.commit();

            selectPreparedStatement = connection.prepareStatement(selectSql);
            ResultSet rs = selectPreparedStatement.executeQuery();
            if (rs.next()) {
                return  rs.getInt("id");
            }
            selectPreparedStatement.close();
            connection.commit();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Delete this line and implement the method.");
    }
}