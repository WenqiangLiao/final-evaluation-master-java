package org.tw.battle.domain.repositories;

import org.tw.battle.domain.ServiceConfiguration;
import org.tw.battle.infrastructure.DatabaseConnectionProvider;

import java.sql.*;

/**
 * @author Liu Xia
 */
public class CharacterRepository {
    private final ServiceConfiguration configuration;

    public CharacterRepository(ServiceConfiguration configuration) {
        this.configuration = configuration;
    }

    public int create(String name, int hp, int x, int y) throws Exception {
        // TODO:
        //   Please implement the method.
        //   Please refer to DatabaseConnectionProvider to see how to create connection.
        String insertSql = "insert into character" + "(name, hp, x, y) values" + "(?, ?, ?, ?)";

        try (
            Connection connection = DatabaseConnectionProvider.createConnection(configuration);
            PreparedStatement insertPreparedStatement = connection.prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);
            )
        {
            connection.setAutoCommit(false);
            insertPreparedStatement.setString(1, name);
            insertPreparedStatement.setInt(2, hp);
            insertPreparedStatement.setInt(3, x);
            insertPreparedStatement.setInt(4, y);
            insertPreparedStatement.execute();

            connection.commit();
            final ResultSet keys = insertPreparedStatement.getGeneratedKeys();
            keys.next();
            return (int)keys.getInt(1);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Delete this line and implement the method.");
    }
}
