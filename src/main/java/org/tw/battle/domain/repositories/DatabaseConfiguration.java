package org.tw.battle.domain.repositories;

import org.tw.battle.domain.ServiceConfiguration;

public class DatabaseConfiguration implements ServiceConfiguration {
    private  String url;
    private  String username;
    private  String password;
    private  String driver;

    public DatabaseConfiguration(String url, String username, String password, String driver) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.driver = driver;
    }

    @Override
    public String getUri() {
        return this.url;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getDriver() {
        return this.driver;
    }
}
