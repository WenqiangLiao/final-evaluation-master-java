package org.tw.battle.domain.repositories;

import org.tw.battle.domain.ServiceConfiguration;
import org.tw.battle.infrastructure.DatabaseConnectionProvider;

import java.sql.*;

public class CharacterCount {
    private final ServiceConfiguration configuration;

    public CharacterCount(ServiceConfiguration configuration) {
        this.configuration = configuration;
    }

    public int count() throws Exception {
        int count = 0;
        String selectSql = "Select COUNT(*) from character";
        try (
                Connection connection = DatabaseConnectionProvider.createConnection(configuration);
                Statement selectStatement = connection.createStatement();)
        {
            connection.setAutoCommit(false);
            ResultSet rs = selectStatement.executeQuery(selectSql);
            while (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

}

